ARG PYTHON_VERSION
FROM python:${PYTHON_VERSION}-slim

SHELL ["/bin/bash", "-c"]

RUN apt-get update && apt-get install -y \
    curl \
    gcc \
    git \
    libffi-dev \
    make \
    rsync \
    && rm -rf /var/lib/apt/lists/* \
    # https://python-poetry.org
    && mkdir /opt/poetry /opt/poetry_cache \
    && curl -sSL https://install.python-poetry.org | POETRY_HOME=/opt/poetry python3 - \
    && git config --global user.name "Max Ludwig" \
    && git config --global user.email "mail@danjou.dev"

ENV PATH="/opt/poetry/bin:$PATH"
ENV POETRY_CACHE_DIR="/opt/poetry_cache"
ENV POETRY_VIRTUALENVS_CREATE="false"

ENV ENV="ci"
WORKDIR /usr/src/myapp
COPY pyproject.toml pyproject.toml
COPY poetry.lock poetry.lock
RUN poetry install --no-root --extras all
