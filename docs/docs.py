#!/usr/bin/env python3

import json
import os
import subprocess
from pathlib import Path
from string import Template

import fs
from dulwich import porcelain as git
from fs.copy import copy_fs, copy_dir
from fs.mountfs import MountFS
from fs.multifs import MultiFS
from fs.osfs import OSFS
from fs.tempfs import TempFS
from packaging.version import Version, InvalidVersion


def parse_tags(tags):
    for tag in tags:
        value = tag.decode() if isinstance(tag, bytes) else tag
        try:
            yield Version(value), value
        except InvalidVersion:
            continue


def create():
    source_fs = OSFS(str(source_path := Path.cwd()))
    output_fs = source_fs.makedirs("public", recreate=True)
    output_fs.removetree("/")

    with MultiFS() as pages_fs:
        pages_fs.add_fs("scaffold", source_fs.opendir("docs/scaffold"))
        pages_fs.add_fs("versions", versions_fs := MountFS())
        versions = [original for _, original in sorted(parse_tags(git.tag_list(source_fs.root_path)))]
        latest = max(versions, default="dev")
        versions.append("dev")
        for version in versions:
            ref = version if version != "dev" else "HEAD"
            version_fs = fs.open_fs(f"gitfs://{source_fs.root_path}").opendir(ref)
            source_fs.removetree("src")
            copy_dir(version_fs, "src", source_fs, "src")
            versions_fs.mount(version, docs_fs := TempFS("docs"))
            subprocess.run(
                [
                    "poetry",
                    "run",
                    "pdoc",
                    "--output-directory",
                    docs_fs.root_path,
                    "--docformat",
                    "numpy",
                    "--edit-url",
                    "codefs=https://gitlab.com/dAnjou/fs-code/-/blob/main/src/codefs/",
                    "--no-math",
                    "--no-search",
                    "--show-source",
                    "--template-directory",
                    source_path / "docs/templates",
                    "codefs",
                    "!codefs._core",
                ],
                cwd=source_path / "src",
                env={"FS_CODE_VERSIONS": ",".join(versions), **os.environ},
            )

        copy_fs(pages_fs, output_fs)
        output_fs.writetext("index.html", Template(output_fs.readtext("index.html")).substitute(latest=latest))
        output_fs.writetext("versions.json", json.dumps({"versions": versions}))

    output_fs.tree()


if __name__ == "__main__":
    create()
