if (!String.prototype.startsWith) {
  Object.defineProperty(String.prototype, 'startsWith', {
    value: function(search, rawPos) {
      var pos = rawPos > 0 ? rawPos|0 : 0;
      return this.substring(pos, pos + search.length) === search;
    }
  });
}

document.addEventListener("DOMContentLoaded", function () {
  var versions = document.getElementById('doc-versions');
  var linkPrefix = versions.dataset.linkPrefix ? versions.dataset.linkPrefix : '';

  var request = new XMLHttpRequest();
  request.open('GET', linkPrefix + '/versions.json', true);

  request.onreadystatechange = function() {
    if (this.readyState === 4) {
      if (this.status == 200) {
        var data = JSON.parse(this.responseText);
        data.versions.forEach(function(version){
          var versionPath = linkPrefix + '/' + version;
          var link = '<a href="' + versionPath + '">' + version + '</a>';
          var cls = document.location.pathname.startsWith(versionPath) ? 'current_version' : '';
          versions.innerHTML += '<span class="' + cls + '">' + link + '</span>';
        });
      }
    }
  };

  request.send();
  request = null;
});
