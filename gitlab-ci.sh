#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

function exec_docker() {
cat << END
  --docker-pull-policy=never \
  --env CI_REGISTRY_IMAGE=fs-code \
  --env TAG=latest \
  --env LOCAL_PROJECT_DIR=/fs-code \
  --docker-volumes $PWD:/fs-code
END
}

function exec_shell() {
cat << END
  --env PATH=$PATH
  --env LOCAL_PROJECT_DIR=$PWD
END
}

executor=$1
job=$2
# shellcheck disable=SC2016,SC2046
gitlab-runner exec "$executor" \
  --env EXCLUDE="--exclude .git $(git status --ignored --short | grep '!!' | sed 's/!!/--exclude/' | tr '\n' ' ')" \
  --pre-build-script 'rsync -ah --delete $EXCLUDE $LOCAL_PROJECT_DIR/ $CI_PROJECT_DIR && git add --all && git commit -m "dev"' \
  --post-build-script 'rsync -ah $CI_PROJECT_DIR/public/ $LOCAL_PROJECT_DIR/public || true' \
  $(exec_"$executor") \
  "$job"
