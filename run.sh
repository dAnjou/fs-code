#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

function run() {
  printf "\e[30;42m >>> %s <<< \e[m\n" "$*"
  eval "$@"
}

function run_image() {
  run docker build --build-arg PYTHON_VERSION=3.13 --cache-from fs-code:latest --tag fs-code:latest .
}

function run_docs() {
  run poetry run python -m http.server --directory public --bind 0.0.0.0 8080
}

function run_retest() {
  run poetry install --extras all
  run poetry run pytest --re-record
}

function run_format() {
  run poetry run black .
}

function run_install_gitlab() {
	run rm -rf /tmp/venv
	run python3 -m venv /tmp/venv
	/tmp/venv/bin/pip config --site set "global.extra-index-url" "https://$(PUBLISH_USER_GITLAB):$(PUBLISH_TOKEN_GITLAB)@gitlab.com/api/v4/projects/$(PROJECT_ID)/packages/pypi/simple"
	run /tmp/venv/bin/pip install "fs-code[all]"
}

function run_clean() {
  elements=(
    "__pycache__"
    ".pytest_cache"
    ".mypy_cache"
    ".coverage"
    "htmlcov"
  )
  for element in "${elements[@]}"; do
	  run find . -depth -name \"$element\" -exec rm -rf \"{}\" \\\;
  done
}

if declare -F run_"$1" > /dev/null; then
  run_"$1"
else
  run "$@"
fi
