from datetime import datetime, timezone
from email.utils import format_datetime
from importlib import import_module
from time import sleep
from typing import Any, cast

import pytest
from fs.base import FS
from vcr import VCR  # type: ignore
from vcr.record_mode import RecordMode  # type: ignore

from tests.core import Config


def pytest_addoption(parser: Any) -> None:
    parser.addoption("--re-record", action="store_true", default=False, help="re-record service requests")


@pytest.fixture(autouse=True)
def rate_limit(request: Any) -> None:
    if request.config.getoption("--re-record"):
        sleep(3)


@pytest.fixture
def config(service: str, request: Any) -> Any:
    _config = cast(Config[FS, FS], import_module(f"tests.{service}").config)
    cassette_file = _config.path / f"{request.function.__name__}.yaml"

    record_mode = RecordMode.NONE
    if request.config.getoption("--re-record"):
        record_mode = RecordMode.ALL
        cassette_file.unlink(missing_ok=True)

    def before_record_response(response: Any) -> Any:
        response["headers"]["Date"] = [
            format_datetime(datetime(2021, 10, 8, 20, 22, 15, tzinfo=timezone.utc), usegmt=True)
        ]
        for key, header in set((h.lower(), h) for h in response["headers"]):
            if key not in _config.keep_response_headers:
                response["headers"].pop(header, None)
        return response

    vcr = VCR(
        record_mode=record_mode,
        filter_headers=_config.remove_request_headers,
        decode_compressed_response=True,
        before_record_response=before_record_response,
    )
    with vcr.use_cassette(str(cassette_file)):
        yield _config
