from abc import ABCMeta, abstractmethod
from dataclasses import dataclass
from pathlib import Path
from typing import Generic, TypeVar, List

UserFS = TypeVar("UserFS")
GroupFS = TypeVar("GroupFS")


@dataclass
class Config(Generic[UserFS, GroupFS], metaclass=ABCMeta):
    id: str
    path: Path
    user: str
    group: str
    repo: str
    ref: str
    token: str
    remove_request_headers: List[str]
    keep_response_headers: List[str]

    @staticmethod
    def cassette_path(path: str) -> Path:
        return (Path(path).parent / "cassettes").resolve()

    @abstractmethod
    def anonymous_user_fs(self) -> UserFS:
        pass

    @abstractmethod
    def authenticated_user_fs(self) -> UserFS:
        pass

    @abstractmethod
    def group_fs(self) -> GroupFS:
        pass
