from typing import Any, List

import pytest
from fs.base import FS
from fs.errors import ResourceReadOnly, ResourceNotFound

from tests.core import Config as BaseConfig

Config = BaseConfig[FS, FS]


class TestCommon:
    @pytest.fixture(params=["test_github", "test_gitlab", "test_gitfs"])
    def service(self, request) -> str:
        return request.param

    def test_auth_user(self, config: Config) -> None:
        user_fs = config.authenticated_user_fs()
        repos = user_fs.listdir(".")
        assert len(repos)

    def test_user(self, config: Config) -> None:
        user_fs = config.anonymous_user_fs()
        repos = user_fs.listdir(".")
        assert len(repos)

    def test_group(self, config: Config) -> None:
        group_fs = config.group_fs()
        repos = group_fs.listdir(".")
        assert len(repos)

    def test_full_path(self, config: Config) -> None:
        user_fs = config.authenticated_user_fs()
        readme = user_fs.open(f"{config.repo}/{config.ref}/README.md")
        assert readme.read()

    def test_repo(self, config: Config) -> None:
        user_fs = config.authenticated_user_fs()
        fs = user_fs.opendir(config.repo)
        assert fs.listdir(".") == ["~"]

    def test_default_branch(self, config: Config) -> None:
        user_fs = config.authenticated_user_fs()
        fs = user_fs.opendir(config.repo)
        assert fs.listdir("~") == fs.listdir(config.ref)

    def test_invalid_repo(self, config: Config) -> None:
        with pytest.raises(ResourceNotFound):
            config.authenticated_user_fs().opendir("xxx")

    def test_ref(self, config: Config) -> None:
        user_fs = config.authenticated_user_fs()
        fs = user_fs.opendir(config.repo).opendir(config.ref)
        assert fs

    def test_invalid_ref(self, config: Config) -> None:
        with pytest.raises(ResourceNotFound):
            config.authenticated_user_fs().opendir(config.repo).opendir("xxx")

    def test_files(self, config: Config) -> None:
        ref_fs = config.authenticated_user_fs().opendir(config.repo).opendir(config.ref)
        files = ref_fs.listdir(".")
        assert len(files)

    def test_walk(self, config: Config) -> None:
        repo = "/test"
        default_branch = "/test/~"
        readme = "/test/~/README.md"
        group_fs = config.group_fs()
        paths = []
        for path, info in group_fs.walk.info("/"):
            paths.append(path)
        assert paths == [repo, default_branch, readme]

    def test_readme(self, config: Config) -> None:
        ref_fs = config.authenticated_user_fs().opendir(config.repo).opendir(config.ref)
        readme_path = list(ref_fs.glob("README*"))[0].path
        readme = ref_fs.open(readme_path)
        assert readme.read()

    @pytest.mark.skip("no license in repo yet")
    def test_license(self, config: Config) -> None:
        repo_fs = config.authenticated_user_fs().opendir(config.repo).opendir(config.ref)
        license_path = list(repo_fs.glob("LI?ENSE*"))[0].path
        license = repo_fs.open(license_path)
        assert license.read()

    @pytest.mark.parametrize(
        "method,args", [("makedir", ["foo"]), ("remove", ["foo"]), ("removedir", ["foo"]), ("setinfo", ["foo", {}])]
    )
    def test_user_fs_readonly(self, config: Config, method: str, args: List[Any]) -> None:
        user_fs = config.authenticated_user_fs()
        with pytest.raises(ResourceReadOnly):
            getattr(user_fs, method)(*args)

    @pytest.mark.parametrize(
        "method,args", [("makedir", ["foo"]), ("remove", ["foo"]), ("removedir", ["foo"]), ("setinfo", ["foo", {}])]
    )
    def test_repo_fs_readonly(self, config: Config, method: str, args: List[Any]) -> None:
        repo_fs = config.authenticated_user_fs().opendir(config.repo)
        with pytest.raises(ResourceReadOnly):
            getattr(repo_fs, method)(*args)

    @pytest.mark.parametrize(
        "method,args", [("makedir", ["foo"]), ("remove", ["foo"]), ("removedir", ["foo"]), ("setinfo", ["foo", {}])]
    )
    def test_ref_fs_readonly(self, config: Config, method: str, args: List[Any]) -> None:
        ref_fs = config.authenticated_user_fs().opendir(config.repo).opendir(config.ref)
        with pytest.raises(ResourceReadOnly):
            getattr(ref_fs, method)(*args)
