import fs
from fs.base import FS
from fs.mountfs import MountFS
from fs.wrap import read_only

from tests.core import Config


class GitConfig(Config[FS, FS]):
    def anonymous_user_fs(self) -> FS:
        mfs = MountFS()
        mfs.mount(self.repo, fs.open_fs(f"gitfs://https://gitlab.com/{self.user}/{self.repo}.git"))
        return read_only(mfs)

    def authenticated_user_fs(self) -> FS:
        mfs = MountFS()
        mfs.mount(self.repo, fs.open_fs(f"gitfs://https://gitlab.com/{self.user}/{self.repo}.git"))
        return read_only(mfs)

    def group_fs(self) -> FS:
        mfs = MountFS()
        mfs.mount(self.repo, fs.open_fs(f"gitfs://https://gitlab.com/{self.group}/{self.repo}.git"))
        return read_only(mfs)


config = GitConfig(
    id="git",
    path=GitConfig.cassette_path(__file__),
    user="fs-code-test",
    group="fs-code-group",
    repo="test",
    ref="main",
    token="",
    remove_request_headers=[],
    keep_response_headers=[  # the others change the cassettes unnecessarily
        "content-type",
    ],
)
