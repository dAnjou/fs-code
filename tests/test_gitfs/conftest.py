from io import BytesIO
from pathlib import Path
from typing import Type
from unittest.mock import MagicMock

import pytest
from dulwich.client import GitClient
from fs.tarfs import WriteTarFS


class Git:
    def __init__(self, tmp_path: Path):
        self._tmp_path = tmp_path

    def archive(self, _, outstream: BytesIO, **kwargs):
        tar_ball = self._tmp_path / "foo.tar"
        with WriteTarFS(str(tar_ball)) as tar_fs:
            tar_fs.writetext("file", "")
        outstream.write(tar_ball.read_bytes())


@pytest.fixture
def git() -> Type[Git]:
    return Git


@pytest.fixture
def client() -> GitClient:
    return MagicMock()
