from pathlib import Path

from dulwich import porcelain
from dulwich.client import get_transport_and_path
from dulwich.repo import MemoryRepo, Repo

from codefs.gitfs import RepoFS


def test_archive_fetcher_with_cloned_repo(pytestconfig, mocker) -> None:
    # as soon as dulwich fixes https://github.com/jelmer/dulwich/issues/1179 this test will fail
    # then I can remove the workaround and this test
    repo_path = str(pytestconfig.invocation_params.dir)
    client, path = get_transport_and_path(repo_path)
    spy_clone = mocker.spy(client, "clone")

    result = RepoFS(client, path).listdir("HEAD")

    assert result
    assert spy_clone.call_count == 2

    for call_index, (repo_class, shallow) in enumerate(
        [
            (Repo, dict(depth=1)),
            (Repo, {}),
        ]
    ):
        (path, _), kwargs = spy_clone.call_args_list[call_index]
        assert path == repo_path
        assert shallow.items() <= kwargs.items()


def test_archive_fetcher_with_shallow(mocker, tmp_path: Path) -> None:
    # as soon as dulwich supports shallow fetches for local repos this test will fail
    # then I can remove the workaround and this test
    porcelain.init(repo := str(tmp_path))
    (test_file := tmp_path / "file").write_text("something")
    porcelain.add(repo, str(test_file))
    porcelain.commit(repo, message="test")

    client, path = get_transport_and_path(str(tmp_path))
    spy_clone = mocker.spy(client, "clone")

    result = RepoFS(client, path).listdir("HEAD")

    assert result
    assert spy_clone.call_count == 2

    for call_index, (repo_class, shallow) in enumerate(
        [
            (Repo, dict(depth=1)),
            (Repo, {}),
        ]
    ):
        (path, _), kwargs = spy_clone.call_args_list[call_index]
        assert path == str(tmp_path)
        assert shallow.items() <= kwargs.items()
