from pathlib import Path
from unittest.mock import MagicMock

import pytest
from dulwich import porcelain
from dulwich.refs import DictRefsContainer
from fs.errors import ResourceNotFound

from codefs.gitfs import RepoFS


@pytest.mark.parametrize(
    "refs,ref",
    [
        ({b"HEAD": b"x"}, "HEAD"),
        ({b"refs/heads/foo": b"x"}, "foo"),
        ({b"refs/tags/foo": b"x"}, "foo"),
    ],
)
def test_valid(monkeypatch, tmp_path: Path, git, client, refs, ref) -> None:
    client.clone = MagicMock(return_value=MagicMock(refs=DictRefsContainer(refs)))

    monkeypatch.setattr(porcelain, "archive", git(tmp_path).archive)

    result = RepoFS(client, "/").listdir(ref)

    assert result == ["file"]


@pytest.mark.parametrize(
    "refs,ref",
    [
        ({b"refs/heads/foo": b"x", b"refs/tags/foo": b"x"}, "foo"),
        ({}, "foo"),
    ],
)
def test_invalid(client, refs, ref) -> None:
    client.clone = MagicMock(return_value=MagicMock(refs=DictRefsContainer(refs)))

    with pytest.raises(ResourceNotFound, match=f"resource '{ref}' not found"):
        RepoFS(client, "/").listdir(ref)
