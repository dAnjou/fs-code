from typing import Any, Mapping

import fs
import pytest
from fs.opener.errors import NotWriteable


def test_opener_succeeds() -> None:
    assert fs.open_fs(f"gitfs://https://example.com")


@pytest.mark.parametrize("arg", [{"writeable": True}, {"create": True}])
def test_opener_not_writable(arg: Mapping[str, Any]) -> None:
    with pytest.raises(NotWriteable):
        fs.open_fs(f"gitfs://", **arg)
