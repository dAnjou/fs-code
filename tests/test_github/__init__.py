import os

from github import Github

from codefs.githubfs import UserFS, OrgFS
from tests.core import Config


class GitHubConfig(Config[UserFS, OrgFS]):
    def anonymous_user_fs(self) -> UserFS:
        return UserFS(Github(), self.user)

    def authenticated_user_fs(self) -> UserFS:
        return UserFS(Github(login_or_token=self.token))

    def group_fs(self) -> OrgFS:
        return OrgFS(Github(), self.group)


config = GitHubConfig(
    id="github",
    path=GitHubConfig.cassette_path(__file__),
    user="fs-code-test",
    group="fs-code-group",
    repo="test",
    ref="main",
    token=os.getenv("FS_CODE_GITHUB_TOKEN", "dummytoken"),
    remove_request_headers=["Authorization", "User-Agent"],
    keep_response_headers=[  # the others change the cassettes unnecessarily
        "location",
    ],
)
