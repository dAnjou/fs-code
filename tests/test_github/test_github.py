import pytest

from tests.core import Config


class TestGitHub:
    @pytest.fixture
    def service(self) -> str:
        return "test_github"

    @pytest.mark.skip("removed support access to repo from RepoFS")
    def test_issues(self, config: Config) -> None:
        repo_fs = config.UserFS(token=config.token).opendir(config.repo)
        issue = [issue for issue in repo_fs.repo.get_issues(state="open") if issue.title == "Test"][0]
        assert issue.get_comments()
