from typing import Any, Mapping

import fs
import pytest
from fs.opener.errors import OpenerError, NotWriteable


@pytest.mark.parametrize(
    "params",
    [
        "token=token",
        "login=login&password=password",
    ],
)
def test_opener_succeeds(params: str) -> None:
    assert fs.open_fs(f"github://?user=user&{params}")


def test_opener_with_host() -> None:
    assert fs.open_fs(f"github://https://example.com?user=user")


@pytest.mark.parametrize("arg", [{"writeable": True}, {"create": True}])
def test_opener_not_writable(arg: Mapping[str, Any]) -> None:
    with pytest.raises(NotWriteable):
        fs.open_fs(f"github://?user=user", **arg)


@pytest.mark.parametrize(
    "params,error",
    [
        ("token=", "must be non-empty string"),
        ("login=&password=password", "must be non-empty string"),
        ("login=login&password=", "must be non-empty string"),
        ("login=&password=", "must be non-empty string"),
        ("login=login", "must pass password when passing login"),
        ("password=password", "must pass login when passing password"),
        ("token=token&login=login", "cannot pass login or password when passing token"),
        ("token=token&password=password", "cannot pass login or password when passing token"),
    ],
)
def test_opener_fails(params: str, error: str) -> None:
    with pytest.raises(OpenerError, match=error):
        fs.open_fs(f"github://?user=user&{params}")
