import os

from gitlab import Gitlab  # type: ignore

from codefs.gitlabfs import UserFS, GroupFS
from tests.core import Config


class GitLabConfig(Config[UserFS, GroupFS]):
    def anonymous_user_fs(self) -> UserFS:
        return UserFS(Gitlab(), self.user)

    def authenticated_user_fs(self) -> UserFS:
        return UserFS(Gitlab(private_token=self.token))

    def group_fs(self) -> GroupFS:
        return GroupFS(Gitlab(), self.group)


config = GitLabConfig(
    id="gitlab",
    path=GitLabConfig.cassette_path(__file__),
    user="fs-code-test",
    group="fs-code-group",
    repo="test",
    ref="main",
    token=os.getenv("FS_CODE_GITLAB_TOKEN", "dummytoken"),
    remove_request_headers=["PRIVATE-TOKEN", "User-Agent"],
    keep_response_headers=[  # the others change the cassettes unnecessarily
        "content-type",
    ],
)
