import pytest

from tests.core import Config


class TestGitLab:
    @pytest.fixture
    def service(self) -> str:
        return "test_gitlab"

    @pytest.mark.skip("removed support access to repo from RepoFS")
    def test_issues(self, config: Config) -> None:
        repo_fs = config.UserFS(token=config.token).opendir(config.repo)
        issue = [issue for issue in repo_fs.repo.issues.list(state="opened") if issue.title == "Test"][0]
        assert list(issue.notes.list())
