from typing import Any, Mapping

import fs
import pytest
from fs.opener.errors import OpenerError, NotWriteable


@pytest.mark.parametrize(
    "params",
    [
        "private_token=private_token",
        "oauth_token=oauth_token",
        "job_token=job_token",
        "http_username=http_username&http_password=http_password",
    ],
)
def test_opener_succeeds(params: str) -> None:
    assert fs.open_fs(f"gitlab://?user=user&{params}")


def test_opener_with_host() -> None:
    assert fs.open_fs(f"gitlab://https://example.com?user=user")


@pytest.mark.parametrize("arg", [{"writeable": True}, {"create": True}])
def test_opener_not_writable(arg: Mapping[str, Any]) -> None:
    with pytest.raises(NotWriteable):
        fs.open_fs(f"gitlab://?user=user", **arg)


@pytest.mark.parametrize(
    "params,error",
    [
        ("private_token=", "must be non-empty string"),
        ("oauth_token=", "must be non-empty string"),
        ("job_token=", "must be non-empty string"),
        ("http_username=&http_password=http_password", "must be non-empty string"),
        ("http_username=http_username&http_password=", "must be non-empty string"),
        ("http_username=&http_password=", "must be non-empty string"),
        ("http_username=http_username", "must pass http_password when passing http_username"),
        ("http_password=http_password", "must pass http_username when passing http_password"),
    ],
)
def test_opener_fails(params: str, error: str) -> None:
    with pytest.raises(OpenerError, match=error):
        fs.open_fs(f"gitlab://?user=user&{params}")


@pytest.mark.parametrize("param", ["http_username", "http_password"])
@pytest.mark.parametrize("token", ["private_token", "oauth_token", "job_token"])
def test_opener_fails_again(token: str, param: str) -> None:
    with pytest.raises(OpenerError, match=f"cannot pass http_username or http_password when passing {token}"):
        fs.open_fs(f"gitlab://?user=user&{token}=foo&{param}=bar")
